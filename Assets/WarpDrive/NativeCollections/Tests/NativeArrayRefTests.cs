using System.Collections;
using NUnit.Framework;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine.TestTools;

namespace WarpDrive.NativeCollections.Tests {
    public class NativeArrayRefTests {
        
        [UnityTest]
        public IEnumerator NativeArrayRef_Sync() {
            var arrays = new NativeArray<NativeArrayRef<int>>(10, Allocator.Persistent);
            var create = new CreateArrayPointerJob {arrays = arrays};
            var handle = create.Schedule();
            handle.Complete();
            
            yield return null;
            
            var results = new NativeList<int>(Allocator.TempJob);
            var read = new ReadArrayPointerJob {arrays = arrays, results = results};
            handle = read.Schedule();
            handle.Complete();
            for (var i = 0; i < 10; i++) {
                for (var j = 0; j < 10; j++) {
                    Assert.AreEqual(i * j, results[i * 10 + j]);
                }
            }
            
            arrays.Dispose();
            results.Dispose();
        }
        
        [UnityTest]
        public IEnumerator NativeArrayRef_Rent() {
            var arrays = new NativeArray<NativeArrayRef<int>>(10, Allocator.Persistent);
            var create = new CreateArrayPointerJob {arrays = arrays};
            var handle = create.Schedule();
            handle.Complete();
            
            yield return null;
            
            var results = new NativeList<int>(Allocator.Persistent);

            for (var t = 0; t < 2; t++) {
                var readRent = new RentReadArrayPointerJob {arrays = arrays, results = results};
                handle = readRent.Schedule();
                handle.Complete();
                for (var i = 0; i < 10; i++) {
                    for (var j = 0; j < 10; j++) {
                        Assert.AreEqual(i * j, results[i * 10 + j]);
                    }
                }
                results.Clear();
            }
            
            var read = new ReadArrayPointerJob {arrays = arrays, results = results};
            handle = read.Schedule();
            handle.Complete();
            for (var i = 0; i < 10; i++) {
                for (var j = 0; j < 10; j++) {
                    Assert.AreEqual(i * j, results[i * 10 + j]);
                }
            }
            
            arrays.Dispose();
            results.Dispose();
        }

        private struct CreateArrayPointerJob : IJob {
            [WriteOnly] public NativeArray<NativeArrayRef<int>> arrays;
        
            public void Execute() {
                for (var i = 0; i < arrays.Length; i++) {
                    var array = new NativeArray<int>(10,Allocator.Temp);
                    for (var j = 0; j < 10; j++) {
                        array[j] = i * j;
                    }

                    arrays[i] = array.ToRef();
                }
            }
        }
        
        [BurstCompile]
        private struct ReadArrayPointerJob : IJob {
            [ReadOnly] public NativeArray<NativeArrayRef<int>> arrays;
            [WriteOnly] public NativeList<int> results;
        
            public void Execute() {
                for (var i = 0; i < arrays.Length; i++) {
                    var array = arrays[i].Unwrap();
                    for (var j = 0; j < array.Length; j++) {
                        results.Add(array[j]);
                    }

                    array.Dispose();
                }
            }
        }
        
        [BurstCompile]
        private struct RentReadArrayPointerJob : IJob {
            [ReadOnly] public NativeArray<NativeArrayRef<int>> arrays;
            [WriteOnly] public NativeList<int> results;
        
            public void Execute() {
                for (var i = 0; i < arrays.Length; i++) {
                    var array = arrays[i].Rent();
                    for (var j = 0; j < array.Length; j++) {
                        results.Add(array[j]);
                    }
                }
            }
        }
    }
}