using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace WarpDrive.NativeCollections {
    public readonly unsafe struct NativeArrayRef<T>
        where T : struct {

        private readonly void* ptr;
        private readonly int length;
        private readonly Allocator alloc;

        /// <summary>
        /// Copy the content to a new memory region and return the reference.
        /// Note that the NativeArray will be disposed.
        /// </summary>
        /// <param name="from">The NativeArray of which reference will be made.</param>
        /// <param name="alloc">Original allocator.</param>
        public NativeArrayRef(ref NativeArray<T> from, Allocator alloc = Allocator.Persistent) {
            this.alloc = alloc;
            length = from.Length;
            ptr = UnsafeUtility.Malloc(UnsafeUtility.SizeOf<T>() * length + sizeof(int), UnsafeUtility.AlignOf<T>(),
                alloc);
            UnsafeUtility.MemCpy((int*) ptr + 1, from.GetUnsafePtr(), UnsafeUtility.SizeOf<T>() * length);
            *(int*) ptr = 0;
            from.Dispose();
            from = default;
        }

        /// <summary>
        /// Unwraps the reference and returns a NativeArray with the original allocator.
        /// You need to call Dispose() after you have done your task with the returned array.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NativeArrayRefDisposedException">The reference is already unwrapped</exception>
        public NativeArray<T> Unwrap() {
            if (*(int*) ptr != 0) throw new NativeArrayRefDisposedException();
            *(int*) ptr = 1;
            var array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>((int*) ptr + 1, length, alloc);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref array, AtomicSafetyHandle.Create());
#endif
            return array;
        }

        /// <summary>
        /// Gets a representation of the array, but does not release the ownership.
        /// Note that there can be more than two references that points to the same array,
        /// which may cause unexpected changes of array contents.
        /// Calling Rent() multiple times is allowed.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public NativeSlice<T> Rent() {
            if (*(int*) ptr != 0) throw new NativeArrayRefDisposedException();
            var array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>((int*) ptr + 1, length,
                Allocator.None);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref array, AtomicSafetyHandle.GetTempMemoryHandle());
#endif
            return new NativeSlice<T>(array);
        }
    }
    
    public static class NativeArrayRefExtensions {
        public static NativeArrayRef<T> ToRef<T>(this NativeArray<T> array) where T : struct {
            return new NativeArrayRef<T>(ref array);
        }
    }

    public class NativeArrayRefDisposedException : Exception {
        
    }
}